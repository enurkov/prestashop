<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{blocknewproducts}prestashop>blocknewproducts_cc68052332d758d991b64087c6e4352e'] = 'Показва сегмент с последно добавените продукти';
$_MODULE['<{blocknewproducts}prestashop>blocknewproducts_73293a024e644165e9bf48f270af63a0'] = 'Невалиден номер.';
$_MODULE['<{blocknewproducts}prestashop>blocknewproducts_e451e6943bb539d1bd804d2ede6474b5'] = 'Изобразени продукти';
$_MODULE['<{blocknewproducts}prestashop>blocknewproducts_b9f5c797ebbf55adccdd8539a65a0241'] = 'Изключено';
$_MODULE['<{blocknewproducts}prestashop>blocknewproducts_9ff0635f5737513b1a6f559ac2bff745'] = 'Нови продукти';
$_MODULE['<{blocknewproducts}prestashop>blocknewproducts_60efcc704ef1456678f77eb9ee20847b'] = 'Всички нови продукти';
$_MODULE['<{blocknewproducts}prestashop>blocknewproducts_c888438d14855d7d96a2724ee9c306bd'] = 'Обновени настройки';
$_MODULE['<{blocknewproducts}prestashop>blocknewproducts_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Включено';
$_MODULE['<{blocknewproducts}prestashop>blocknewproducts_c9cc8cce247e49bae79f15173ce97354'] = 'Запази';
$_MODULE['<{blocknewproducts}prestashop>blocknewproducts_f7c34fc4a48bc683445c1e7bbc245508'] = 'Нови продукти сегмент';
$_MODULE['<{blocknewproducts}prestashop>blocknewproducts_530c88f8210e022b39128e3f0409bbcf'] = 'Винаги показвай блока';
$_MODULE['<{blocknewproducts}prestashop>blocknewproducts_f4f70727dc34561dfde1a3c529b6205c'] = 'Настройки';
$_MODULE['<{blocknewproducts}prestashop>blocknewproducts_cc4bbebd6a8d2fb633a1dd8ceda3fc8d'] = 'Задайте номера на продукти които ще бъдат изобразени в този сегмент';
