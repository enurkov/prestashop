<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{blocktopmenu}prestashop>blocktopmenu_b021df6aac4654c454f46c77646e745f'] = 'Етикет';
$_MODULE['<{blocktopmenu}prestashop>blocktopmenu_7a115fe2bd8b94ead1550a2da953cf87'] = 'Нов прозорец';
$_MODULE['<{blocktopmenu}prestashop>blocktopmenu_5c99ba0242e549e14a00ad361de435d1'] = 'Добави';
$_MODULE['<{blocktopmenu}prestashop>blocktopmenu_004bf6c9a40003140292e97330236c53'] = 'Действие';
$_MODULE['<{blocktopmenu}prestashop>blocktopmenu_bafd7322c6e97d25b6299b5d6fe8920b'] = 'Не';
$_MODULE['<{blocktopmenu}prestashop>blocktopmenu_597582c140dd691b522fe42299a24d34'] = 'Обновени настройки';
$_MODULE['<{blocktopmenu}prestashop>blocktopmenu_fb9ddcf8de4f32e4db61221d5cebb108'] = 'Магазин';
$_MODULE['<{blocktopmenu}prestashop>blocktopmenu_f4f70727dc34561dfde1a3c529b6205c'] = 'Настройки';
$_MODULE['<{blocktopmenu}prestashop>blocktopmenu_ec136b444eede3bc85639fac0dd06229'] = 'Доставчик';
$_MODULE['<{blocktopmenu}prestashop>blocktopmenu_af1b98adf7f686b84cd0b443e022b7a0'] = 'Категории';
$_MODULE['<{blocktopmenu}prestashop>blocktopmenu_93cba07454f06a4a960172bbd6e2a435'] = 'Да';
$_MODULE['<{blocktopmenu}prestashop>blocktopmenu_540c8f06b80b4ec911505764eb455f4d'] = 'Всички магазини';
$_MODULE['<{blocktopmenu}prestashop>blocktopmenu_068f80c7519d0528fb08e82137a72131'] = 'Продукти';
$_MODULE['<{blocktopmenu}prestashop>blocktopmenu_ec211f7c20af43e742bf2570c3cb84f9'] = 'Добави';
$_MODULE['<{blocktopmenu}prestashop>blocktopmenu_1063e38cb53d94d386f21227fcd84717'] = 'Премахни';
$_MODULE['<{blocktopmenu}prestashop>blocktopmenu_97f08a40f22a625d0cbfe03db3349108'] = 'ID на продукта';
$_MODULE['<{blocktopmenu}prestashop>blocktopmenu_97e7c9a7d06eac006a28bf05467fcc8b'] = 'Връзка';
$_MODULE['<{blocktopmenu}prestashop>blocktopmenu_4994a8ffeba4ac3140beb89e8d41f174'] = 'Език';
$_MODULE['<{blocktopmenu}prestashop>blocktopmenu_7dce122004969d56ae2e0245cb754d35'] = 'Промени';
$_MODULE['<{blocktopmenu}prestashop>blocktopmenu_9dea4016dbcc290b773ab2fae678aaa8'] = 'продукти: ';
$_MODULE['<{blocktopmenu}prestashop>blocktopmenu_c0bd7654d5b278e65f21cf4e9153fdb4'] = 'Производител';
$_MODULE['<{blocktopmenu}prestashop>blocktopmenu_dbd7201170abec94907af0cc1680d22d'] = 'Запази';
$_MODULE['<{blocktopmenu}prestashop>blocktopmenu_12a521af593422cd508f7707662c9eb2'] = 'Магазини';
