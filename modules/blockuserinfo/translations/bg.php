<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{blockuserinfo}prestashop>blockuserinfo_12641546686fe11aaeb3b3c43a18c1b3'] = 'Вашата Количка';
$_MODULE['<{blockuserinfo}prestashop>blockuserinfo_83218ac34c1834c26781fe4bde918ee4'] = 'Добре дошли';
$_MODULE['<{blockuserinfo}prestashop>blockuserinfo_4394c8d8e63c470de62ced3ae85de5ae'] = 'Изход';
$_MODULE['<{blockuserinfo}prestashop>blockuserinfo_9e65b51e82f2a9b9f72ebe3e083582bb'] = '(празна)';
$_MODULE['<{blockuserinfo}prestashop>blockuserinfo_a0623b78a5f2cfe415d9dbbd4428ea40'] = 'Вашият Профил';
$_MODULE['<{blockuserinfo}prestashop>blockuserinfo_a2e9cd952cda8ba167e62b25a496c6c1'] = 'Сегмент Детайли за клиент';
$_MODULE['<{blockuserinfo}prestashop>blockuserinfo_970a31aa19d205f92ccfd1913ca04dc0'] = 'добави сегмент който показва информация за потребител';
$_MODULE['<{blockuserinfo}prestashop>blockuserinfo_f5bf48aa40cad7891eb709fcf1fde128'] = 'продукт';
$_MODULE['<{blockuserinfo}prestashop>blockuserinfo_86024cad1e83101d97359d7351051156'] = 'продукти';
$_MODULE['<{blockuserinfo}prestashop>blockuserinfo_bffe9a3c9a7e00ba00a11749e022d911'] = 'Вход';
$_MODULE['<{blockuserinfo}prestashop>blockuserinfo_7fc68677a16caa0f02826182468617e6'] = 'Количка';
$_MODULE['<{blockuserinfo}prestashop>blockuserinfo_4b877ba8588b19f1b278510bf2b57ebb'] = 'Изключи ме';
