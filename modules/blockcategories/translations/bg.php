<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{blockcategories}prestashop>blockcategories_8f0ed7c57fca428f7e3f8e64d2f00918'] = 'сегмент Категории';
$_MODULE['<{blockcategories}prestashop>blockcategories_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Включен';
$_MODULE['<{blockcategories}prestashop>blockcategories_2c1199ee18b344152f35ec237acb5796'] = 'Активен динамичен (анимиран) режим на поднивата';
$_MODULE['<{blockcategories}prestashop>blockcategories_883f0bd41a4fcee55680446ce7bec0d9'] = 'По позиция';
$_MODULE['<{blockcategories}prestashop>blockcategories_54e4f98fb34254a6678f0795476811ed'] = 'По име';
$_MODULE['<{blockcategories}prestashop>blockcategories_footer_af1b98adf7f686b84cd0b443e022b7a0'] = 'Категории';
$_MODULE['<{blockcategories}prestashop>blockcategories_15a6f5841d9e4d7e62bec3319b4b7036'] = 'Добавя сегмент включващ категориите на продуктите';
$_MODULE['<{blockcategories}prestashop>blockcategories_c888438d14855d7d96a2724ee9c306bd'] = 'Обновени настройки';
$_MODULE['<{blockcategories}prestashop>blockcategories_6b46ae48421828d9973deec5fa9aa0c3'] = 'Подреждане';
$_MODULE['<{blockcategories}prestashop>blockcategories_23e0d4ecc25de9b2777fdaca3e2f3193'] = 'Максимална дълбочина: Невалиден номер. ';
$_MODULE['<{blockcategories}prestashop>blockcategories_0cf328636f0d607ac24a5c435866b94b'] = 'Динамичен HTML : Невалиден избор.';
$_MODULE['<{blockcategories}prestashop>blockcategories_f4f70727dc34561dfde1a3c529b6205c'] = 'Настройки';
$_MODULE['<{blockcategories}prestashop>blockcategories_19561e33450d1d3dfe6af08df5710dd0'] = 'Максимална дълбочина';
$_MODULE['<{blockcategories}prestashop>blockcategories_ef35cd8f1058f29151991e9ca94b36fb'] = 'задайте максималната дълбочина на поднивата в този сегмент (0 = безкрайност)';
$_MODULE['<{blockcategories}prestashop>blockcategories_b9f5c797ebbf55adccdd8539a65a0241'] = 'Изключен';
$_MODULE['<{blockcategories}prestashop>blockcategories_cf3fb1ff52ea1eed3347ac5401ee7f0c'] = 'Увеличващ';
$_MODULE['<{blockcategories}prestashop>blockcategories_971fd8cc345d8bd9f92e9f7d88fdf20c'] = 'Динамичен';
$_MODULE['<{blockcategories}prestashop>blockcategories_c9cc8cce247e49bae79f15173ce97354'] = 'Запази';
$_MODULE['<{blockcategories}prestashop>blockcategories_af1b98adf7f686b84cd0b443e022b7a0'] = 'Категории';
