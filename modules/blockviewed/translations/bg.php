<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{blockviewed}prestashop>blockviewed_2e57399079951d84b435700493b8a8c1'] = 'Трябва да попълните в полето \'Показани Продукти\'';
$_MODULE['<{blockviewed}prestashop>blockviewed_8ce2b5b3ffa27a8a66220a49357a1582'] = 'Определете номер на продуктите които да се показват в тази част.';
$_MODULE['<{blockviewed}prestashop>blockviewed_c9cc8cce247e49bae79f15173ce97354'] = 'Запиши';
$_MODULE['<{blockviewed}prestashop>blockviewed_fad87bb9a90d9e42854af13633aac252'] = 'Добавя сегмент показващ последно разглежданите продукти';
$_MODULE['<{blockviewed}prestashop>blockviewed_49fa2426b7903b3d4c89e2c1874d9346'] = 'Още за';
$_MODULE['<{blockviewed}prestashop>blockviewed_73293a024e644165e9bf48f270af63a0'] = 'невалиден номер';
$_MODULE['<{blockviewed}prestashop>blockviewed_c888438d14855d7d96a2724ee9c306bd'] = 'Обновени настройки';
$_MODULE['<{blockviewed}prestashop>blockviewed_f4f70727dc34561dfde1a3c529b6205c'] = 'Настройки';
$_MODULE['<{blockviewed}prestashop>blockviewed_859e85774d372c6084d62d02324a1cc3'] = 'Разглеждани продукти поле';
$_MODULE['<{blockviewed}prestashop>blockviewed_e451e6943bb539d1bd804d2ede6474b5'] = 'Показани продукти';
$_MODULE['<{blockviewed}prestashop>blockviewed_43560641f91e63dc83682bc598892fa1'] = 'Разглеждани продукти';
