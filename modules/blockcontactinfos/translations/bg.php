<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{blockcontactinfos}prestashop>blockcontactinfos_6bde6531cb3dc89a517c203d2177315d'] = 'Блок Информация за контакт';
$_MODULE['<{blockcontactinfos}prestashop>blockcontactinfos_dd7bf230fde8d4836917806aff6a6b27'] = 'Адрес';
$_MODULE['<{blockcontactinfos}prestashop>blockcontactinfos_ce8ae9da5b7cd6c3df2929543a9af92d'] = 'Намалени цени';
$_MODULE['<{blockcontactinfos}prestashop>blockcontactinfos_02d4482d332e1aef3437cd61c9bcc624'] = 'Контакти';
$_MODULE['<{blockcontactinfos}prestashop>blockcontactinfos_c281f92b77ba329f692077d23636f5c9'] = 'Име на фирма';
$_MODULE['<{blockcontactinfos}prestashop>blockcontactinfos_d0398e90769ea6ed2823a3857bcc19ea'] = 'Тел.';
$_MODULE['<{blockcontactinfos}prestashop>blockcontactinfos_e4b4279fc7df686f160eaf46e6129f85'] = 'Добавя блок показващ информация за контакт с магазина';
$_MODULE['<{blockcontactinfos}prestashop>blockcontactinfos_20015706a8cbd457cbb6ea3e7d5dc9b3'] = 'Настройките са обновени';
$_MODULE['<{blockcontactinfos}prestashop>blockcontactinfos_1f8261d17452a959e013666c5df45e07'] = 'Телефонен номер';
$_MODULE['<{blockcontactinfos}prestashop>blockcontactinfos_b17f3f4dcf653a5776792498a9b44d6a'] = 'Обнови настройките';
$_MODULE['<{blockcontactinfos}prestashop>blockcontactinfos_6a1e265f92087bb6dd18194833fe946b'] = 'c:';
