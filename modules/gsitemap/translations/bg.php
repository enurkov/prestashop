<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{gsitemap}prestashop>gsitemap_0a6ee1a4f10278211d74152e633d0ed0'] = 'Грешка при създаването на карта на сайта';
$_MODULE['<{gsitemap}prestashop>gsitemap_39d616ecc73e174052f1877ac2b3c853'] = 'Обнови:';
$_MODULE['<{gsitemap}prestashop>gsitemap_8103220a578f92ec726809c3d47adc6e'] = 'Генерирай карта на сайта';
$_MODULE['<{gsitemap}prestashop>gsitemap_f5a592018c8b55d2c968515ad7c58c91'] = 'Оптимизиране за търсачки';
$_MODULE['<{gsitemap}prestashop>gsitemap_4ff2e716a7d06ce5274b4090b39abad3'] = 'Виж';
$_MODULE['<{gsitemap}prestashop>gsitemap_935a6bc13704a117d22333c3155b5dae'] = 'Генерирай своя Google карта на сайта';
$_MODULE['<{gsitemap}prestashop>gsitemap_3f69f5c6c3c90eee482f28ae77390cb9'] = 'Индексирани страници:';
$_MODULE['<{gsitemap}prestashop>gsitemap_abf27597777430142de13ed6eea36d75'] = 'за повече информация';
$_MODULE['<{gsitemap}prestashop>gsitemap_b908c2f34052b5276e0bf50f0e042211'] = 'Рзмер на файла:';
$_MODULE['<{gsitemap}prestashop>gsitemap_be35982b0637fe74565dde00768e3534'] = 'Позлвай cron job за обновяване на sitemap-а:';
$_MODULE['<{gsitemap}prestashop>gsitemap_3aaaafde6f9b2701f9e6eb9292e95521'] = 'Google карта на сайта';
$_MODULE['<{gsitemap}prestashop>gsitemap_eb175747116b689caadea1b84202f513'] = 'Вашата карта на сайта е online на следния адрес:';
$_MODULE['<{gsitemap}prestashop>gsitemap_0033cf1fdbd4354facfaa51f6f0de6a4'] = 'Обнови картата на сайта';
$_MODULE['<{gsitemap}prestashop>gsitemap_8334797b9b3383d4f48d98178b8845ea'] = 'тази страница';
