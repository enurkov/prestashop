<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{pagesnotfound}prestashop>pagesnotfound_3ae7050d9f8502e9b69448a6db73fab2'] = 'Покажи страниците изискани от твоите потребители но не намерени';
$_MODULE['<{pagesnotfound}prestashop>pagesnotfound_64d129224a5377b63e9727479ec987d9'] = 'Брояч';
$_MODULE['<{pagesnotfound}prestashop>pagesnotfound_d372ffc9065cb7d2ea24df137927d060'] = 'Няма регистрирани страници';
$_MODULE['<{pagesnotfound}prestashop>pagesnotfound_535ff57deda0b45d32cb37fd430accc8'] = 'Ако вашият хостинг поддържа .htaccess файл, то може да го създадете в главната директория и да добавите следният ред в него:';
$_MODULE['<{pagesnotfound}prestashop>pagesnotfound_feb436b0dabe28068aa6d866ac47bf0a'] = 'Лог от този модул съдържа: повикани страници, от къде идва потребителят и колко пъти е зареден този адрес.';
$_MODULE['<{pagesnotfound}prestashop>pagesnotfound_a90083861c168ef985bf70763980aa60'] = 'Как да хванете тези грешки ?';
$_MODULE['<{pagesnotfound}prestashop>pagesnotfound_3604249130acf7fda296e16edc996e5b'] = 'грешки 404';
$_MODULE['<{pagesnotfound}prestashop>pagesnotfound_9cfaa8e935f9e3e65b0959b15b070b15'] = 'Ако потребителят въведе несъществуващ адрес от Вашият магазин, то той ще бъде пренасочен към тази страница.';
$_MODULE['<{pagesnotfound}prestashop>pagesnotfound_251295238bdf7693252f2804c8d3707e'] = 'Не е намерена страница';
$_MODULE['<{pagesnotfound}prestashop>pagesnotfound_6cb944288ac528fcfd76b20156dddce1'] = 'Трябва да използвате .htaccess файл за да пренасочите 404 errors към страницата \\"404.php\\"';
$_MODULE['<{pagesnotfound}prestashop>pagesnotfound_193cfc9be3b995831c6af2fea6650e60'] = 'Страница';
$_MODULE['<{pagesnotfound}prestashop>pagesnotfound_b6f05e5ddde1ec63d992d61144452dfa'] = 'Предложил';
$_MODULE['<{pagesnotfound}prestashop>pagesnotfound_6602bbeb2956c035fb4cb5e844a4861b'] = 'Ръководство';
