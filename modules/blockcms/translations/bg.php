<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{blockcms}prestashop>blockcms_cd275312651aebf8cbbb8d3bb1c086ae'] = 'Блок CMS';
$_MODULE['<{blockcms}prestashop>blockcms_cdca12007979fc49008fd125cdb775fc'] = 'Добавя блок с няколко CMS линка';
$_MODULE['<{blockcms}prestashop>blockcms_fca0206c8afc8d67969f9aec20e3287e'] = 'CMS категория';
$_MODULE['<{blockcms}prestashop>blockcms_abf9012e9560b3c44a536f5c4a2a0b62'] = 'местоположение';
$_MODULE['<{blockcms}prestashop>blockcms_945d5e233cf7d6240f6b783b36a374ff'] = 'ляво ';
$_MODULE['<{blockcms}prestashop>blockcms_bafd7322c6e97d25b6299b5d6fe8920b'] = 'Не';
$_MODULE['<{blockcms}prestashop>blockcms_e1da49db34b0bdfdddaba2ad6552f848'] = 'карта на сайта';
$_MODULE['<{blockcms}prestashop>form_b718adec73e04ce3ec720dd11a06a308'] = 'ID';
$_MODULE['<{blockcms}prestashop>form_08a38277b0309070706f6652eeae9a53'] = 'Надолу';
$_MODULE['<{blockcms}prestashop>blockcms_a82be0f551b8708bc08eb33cd9ded0cf'] = 'Информация';
$_MODULE['<{blockcms}prestashop>blockcms_ef61fb324d729c341ea8ab9901e23566'] = 'Добави нов';
$_MODULE['<{blockcms}prestashop>blockcms_c9cc8cce247e49bae79f15173ce97354'] = 'Запамети';
$_MODULE['<{blockcms}prestashop>blockcms_93cba07454f06a4a960172bbd6e2a435'] = 'ДА';
$_MODULE['<{blockcms}prestashop>blockcms_34c869c542dee932ef8cd96d2f91cae6'] = 'Магазини';
$_MODULE['<{blockcms}prestashop>blockcms_d1aa22a3126f04664e0fe3f598994014'] = 'Специални';
$_MODULE['<{blockcms}prestashop>blockcms_02d4482d332e1aef3437cd61c9bcc624'] = 'Контакти';
$_MODULE['<{blockcms}prestashop>form_52f5e0bc3859bc5f5e25130b6c7e8881'] = 'Позиция';
$_MODULE['<{blockcms}prestashop>form_258f49887ef8d14ac268c92b02503aaa'] = 'Нагоре';
$_MODULE['<{blockcms}prestashop>form_7dce122004969d56ae2e0245cb754d35'] = 'Редактирай';
$_MODULE['<{blockcms}prestashop>form_49ee3087348e8d44e1feda1917443987'] = 'Наименование';
$_MODULE['<{blockcms}prestashop>blockcms_87a2663d841b78f01c27c0edb4f50b76'] = 'Успешно изтриване';
$_MODULE['<{blockcms}prestashop>blockcms_9ff0635f5737513b1a6f559ac2bff745'] = 'Нови продукти';
$_MODULE['<{blockcms}prestashop>blockcms_3cb29f0ccc5fd220a97df89dafe46290'] = 'Най-продавани';
$_MODULE['<{blockcms}prestashop>blockcms_5813ce0ec7196c492c97596718f71969'] = 'Карта на сайта';
$_MODULE['<{blockcms}prestashop>blockcms_ea4788705e6873b424c65e91c2846b19'] = 'Откажи';
$_MODULE['<{blockcms}prestashop>blockcms_38fb7d24e0d60a048f540ecb18e13376'] = 'Запази';
$_MODULE['<{blockcms}prestashop>blockcms_92b09c7c48c520c3c55e497875da437c'] = 'дясно';
$_MODULE['<{blockcms}prestashop>blockcms_7a52e36bf4a1caa031c75a742fb9927a'] = 'поддържан с';
$_MODULE['<{blockcms}prestashop>form_06df33001c1d7187fdd81ea1f5b277aa'] = 'Действие';
$_MODULE['<{blockcms}prestashop>form_f2a6c498fb90ee345d997f888fce3b18'] = 'Изтрий';
