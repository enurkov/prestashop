<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{statsforecast}prestashop>statsforecast_03727ac48595a24daed975559c944a44'] = 'ден';
$_MODULE['<{statsforecast}prestashop>statsforecast_7442e29d7d53e549b78d93c46b8cdcfc'] = 'Поръчки';
$_MODULE['<{statsforecast}prestashop>statsforecast_11ff9f68afb6b8b5b8eda218d7c83a65'] = 'Продажби';
$_MODULE['<{statsforecast}prestashop>statsforecast_b1897515d548a960afe49ecf66a29021'] = 'Среден';
$_MODULE['<{statsforecast}prestashop>statsforecast_e55f75a29310d7b60f7ac1d390c8ae42'] = 'Модул';
$_MODULE['<{statsforecast}prestashop>statsforecast_3adbdb3ac060038aa0e6e6c138ef9873'] = 'Категория';
$_MODULE['<{statsforecast}prestashop>statsforecast_e6d0e1c8fc6a4fcf47869df87e04cd88'] = 'Потребители';
$_MODULE['<{statsforecast}prestashop>statsforecast_03937134cedab9078be39a77ee3a48a0'] = 'Групи';
$_MODULE['<{statsforecast}prestashop>statsforecast_f2bbdf9f72c085adc4d0404e370f0f4c'] = 'Атрибут';
$_MODULE['<{statsforecast}prestashop>statsforecast_d2ce009594dcc60befa6a4e6cbeb71fc'] = 'Седмица';
$_MODULE['<{statsforecast}prestashop>statsforecast_9dea4016dbcc290b773ab2fae678aaa8'] = 'продукти: ';
$_MODULE['<{statsforecast}prestashop>statsforecast_fc26e55e0993a75e892175deb02aae15'] = 'Колички';
$_MODULE['<{statsforecast}prestashop>statsforecast_88183b946cc5f0e8c96b2e66e1c74a7e'] = 'Незнаен';
$_MODULE['<{statsforecast}prestashop>statsforecast_537c66b24ef5c83b7382cdc3f34885f2'] = 'Година';
$_MODULE['<{statsforecast}prestashop>statsforecast_d7e637a6e9ff116de2fa89551240a94d'] = 'Посещения';
$_MODULE['<{statsforecast}prestashop>statsforecast_96b0141273eabab320119c467cdcaf17'] = 'Общо';
$_MODULE['<{statsforecast}prestashop>statsforecast_ae5d01b6efa819cc7a7c05a8c57fcc2c'] = 'Посетители';
$_MODULE['<{statsforecast}prestashop>statsforecast_0bcef9c45bd8a48eda1b26eb0c61c869'] = '%';
$_MODULE['<{statsforecast}prestashop>statsforecast_7cbb885aa1164b390a0bc050a64e1812'] = 'Месец';
$_MODULE['<{statsforecast}prestashop>statsforecast_591411cc8927851db2002208676d8330'] = 'Рег.';
$_MODULE['<{statsforecast}prestashop>statsforecast_e6e42855066e7a3ae050b2c698021b14'] = 'Зона:';
$_MODULE['<{statsforecast}prestashop>statsforecast_a85eba4c6c699122b2bb1387ea4813ad'] = 'Количка';
$_MODULE['<{statsforecast}prestashop>statsforecast_b3ff996fe5c77610359114835baf9b38'] = 'Зона';
$_MODULE['<{statsforecast}prestashop>statsforecast_386c339d37e737a436499d423a77df0c'] = 'Валута';
