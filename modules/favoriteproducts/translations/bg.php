<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{favoriteproducts}prestashop>favoriteproducts-extra_77b65985b439ff157c1c13d63c9dc294'] = 'Добави този продукт в любимите ми';
$_MODULE['<{favoriteproducts}prestashop>my-account_a34e0796b9c15d09300f67d972379722'] = 'Моите любими продукти';
$_MODULE['<{favoriteproducts}prestashop>favoriteproducts-account_0b3db27bc15f682e92ff250ebb167d4b'] = 'Назад към Вашият Профил';
$_MODULE['<{favoriteproducts}prestashop>favoriteproducts-account_d95cf4ab2cbf1dfb63f066b50558b07d'] = 'Моят профил';
$_MODULE['<{favoriteproducts}prestashop>favoriteproducts-account_a34e0796b9c15d09300f67d972379722'] = 'Моите любими продукти';
$_MODULE['<{favoriteproducts}prestashop>favoriteproducts-account_e2233f8a7cec20324ed48bc32ec98a5d'] = 'Все още няма любими продукти.';
$_MODULE['<{favoriteproducts}prestashop>favoriteproducts-extra_84934524b3493305f6faf3074258b512'] = 'Премахни този продукт от любимите ми';
